#!/bin/bash
curl -s https://coderbyte.com/api/challenges/logs/web-logs-raw | grep 'heroku/router' | awk -F ' ' '{print $10" "$11}' | sed 's/request_id=//' | sed 's/fwd=//' | sed 's/MASKED/M/' | sed 's/"/[/' | sed 's/"/]/'

