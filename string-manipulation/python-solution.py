import os
import requests

response = requests.get('https://coderbyte.com/api/challenges/logs/web-logs-raw')
with open("response.txt", "w") as f:
    f.write(response.text)
with open('response.txt') as topo_file:
    for line in topo_file:
        if "heroku/router" in line:
            result = line.find('request_id')
            intermediate = line[result+11:(result+68)]  
            intermediate = intermediate.replace('fwd="', '[')
            intermediate = intermediate.replace('dyno=we', '')
            intermediate = intermediate.replace('[MASKED"', '[M]')
            final = intermediate.replace('"', ']')
            print(final)

