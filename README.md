# untitled-project

## Litecoin

* Litecoin docker file is added [here](litecoin/Dockerfile) along with entrypoint.
* For deploying litecoin as statefulset have used [helm charts](litecoin/charts). 
* The pipeline for building litecoin image , pushing it to ecr and deploying on kube cluster is defined in the [gitlab ci file](.gitlab-ci.yml)
![pipeline](assets/pipe.png)
* The first two steps are automated and get triggered upon push to main branch . The deploy step requires manual intervention.
![pipeline](assets/pipe1.png)

## Gitlab Runner

* The gitlab runner uses [this](runner/Dockerfile) for spinning up environments to build and deploy the litecoin . 

## String Manipulation

* The sourced question and its solution for string manipulation is [here](string-manipulation/Readme.md)

## Terraform

* [Module](terraform/modules/iam) to create iam policy , iam user, iam group .

* [Example](terraform/main.tf) on how to use the module.





