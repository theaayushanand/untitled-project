terraform {
  backend "s3" {
    bucket = "dmeg-tech-infra-backend"
    key    = "iam/terraform.tfstate"
    region = "ap-south-1"
  }
}
