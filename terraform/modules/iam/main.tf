
data "aws_iam_policy_document" "iam_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"] 

    principals {
      type        = "AWS"
      identifiers = ["740716721972"] #create an assume role policy document for all users in the account
    }
  }
}


resource "aws_iam_policy" "policy" {
  name        = "${var.name}-policy"
  path        = "/"
  description = "Policy with assume role for all things within the account"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      { Effect = "Allow"
        Action = ["sts:AssumeRole"]
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_role" "iam_role" { #create an iam role with the specified iam policy
  name               = "${var.name}-role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.iam_assume_role_policy.json
}

resource "aws_iam_group" "iam_group" { # create an iam group
  name = "${var.name}-group"
  path = "/"
}


resource "aws_iam_group_policy_attachment" "policy_attach" { # attach the iam group with the policy we created
  group      = aws_iam_group.iam_group.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_iam_user" "iam_user" { # create a user
  name = "${var.name}-user"
  path = "/"

}

resource "aws_iam_group_membership" "group_membership" { # add the user to the group
  name = "${var.name}-group_membership"

  users = [
    aws_iam_user.iam_user.name,
  ]

  group = aws_iam_group.iam_group.name
}
