provider "aws" {
  region = "ap-southeast-1"
}
terraform {
  required_version = ">= 0.12.6"
}
